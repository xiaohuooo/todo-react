import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import { resolve } from 'path'
import AutoImport from 'unplugin-auto-import/vite'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(),
  AutoImport({
    include: [/.[tj]sx?$/],
    imports: ['react', 'react-router-dom'],
    dirs: [],
    dts: true,
    defaultExportByFilename: false
  }),],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      '#': resolve(__dirname, './src/typings')
    }
  },
  base: "https://mdn.github.io/todo-react/"
})
