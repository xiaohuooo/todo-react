import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { router } from "@/routes";
import { RouterProvider } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
const DATA1 = [
  { id: "todo-0", name: "Eat", completed: true },
  { id: "todo-1", name: "Sleep", completed: false },
  { id: "todo-2", name: "Repeat", completed: false },
];
// localStorage.setItem("tasks", JSON.stringify(DATA1));
window.__NETWORK_STATUS__ = "ONLINE";
if ("serviceWorker" in navigator) {
  // navigator.serviceWorker.addEventListener("message", (event) => {
  //   const { type } = event.data;
  //   window.__NETWORK_STATUS__ = type;
  //   console.log('网络状态', window.__NETWORK_STATUS__);
  //   const netChangeEvent = new CustomEvent('networkStatusChange', {
  //     detail: type === 'ONLINE' ? true : false,
  //     bubbles: true,
  //     cancelable: false
  //   });
  //   window.dispatchEvent(netChangeEvent);
  // });
  navigator.serviceWorker
    .register("/service-worker.js")
    .then(() => {
      console.log("Service Worker 注册成功");
    })
    .catch((err) => console.log("Service Worker 注册失败: ", err));
}


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
