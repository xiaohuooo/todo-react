import { createRef } from 'react'
import { createBrowserRouter } from 'react-router-dom'
import Home from '../pages/home'
import Photo from '../pages/photo'
import LayoutWithTabBar from '../layout/WithTabBar/index'

const DATA = JSON.parse(localStorage.getItem("tasks")) || [];
const routesConfig = [
    { path: '/todo-react/', name: 'Home', element: <Home />, nodeRef: createRef() },
    { path: '/todo-react/Photo', name: 'Photo', element: <Photo />, nodeRef: createRef() },
]

export const router = createBrowserRouter([
    {
        path: '/todo-react',
        element: <LayoutWithTabBar />,
        children: routesConfig.map((route) => ({
            index: route.path === '/',
            path: route.path === '/' ? undefined : route.path,
            element: route.element,
        })),
    }
])

export default routesConfig
