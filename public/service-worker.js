self.addEventListener("install", (event) => {
  self.skipWaiting();
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.addAll([...CACHE_FILE_1, ...CACHE_FILE_2]);
    })
  );
});
